
export function collideWithBorder(cr, b, vx, vy) {
  if (cr < 0 || cr > 1) {
    return null; // meaningless
  }
  
  switch(b) {
    case "top":
    case "bottom":
      return [ vx, -1 * cr * vy ];
    case "left":
    case "right":
      return [ -1 * cr * vx, vy ];
    default:
      return null;
  }
}

export function collide(cr, m1, x1, y1, v1x, v1y, m2, x2, y2, v2x, v2y) {
  if (cr < 0 || cr > 1) {
    return null; // meaningless
  }
  
  if (m1 == 0 && m2 == 0) {
    return null; // meaningless
  }
  
  if (m1 == 0 || m2 == 0) {
    return [ v1x, v1y, v2x, v2y ]; // no collision
  }
  
  var vxcm = (m1 * v1x + m2 * v2x) / (m1 + m2);
  var vycm = (m1 * v1y + m2 * v2y) / (m1 + m2);
  
  // boost into the CM frame
  v1x -= vxcm;
  v1y -= vycm;
  
  var dx = x2 - x1;
  var dy = y2 - y1;
  var d2 = dx * dx + dy * dy;
  
  var v1xf = (v1x * (dy * dy - cr * dx * dx) - v1y * (1 + cr) * dx * dy) / d2;
  var v1yf = -1 * (v1x * (1 + cr) * dx * dy - v1y * (dx * dx - cr * dy * dy)) / d2;
  
  v2x = -1 * (m1 / m2) * v1xf + vxcm; // boost back into lab frame
  v2y = -1 * (m1 / m2) * v1yf + vycm;
  
  return [ v1xf + vxcm, v1yf + vycm, v2x, v2y ];
}