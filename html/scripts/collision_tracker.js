
import { nextCollision, nextCollisionWithBorders, getPosition, getVelocity } from './collision_detection.js';
import { collideWithBorder, collide } from './collision.js';

function CollisionTuple(t, i, j) {
  this.t = t;
  this.i = i;
  this.j = j;
}

CollisionTuple.prototype.toString = function() {
  return "(" + this.t + "," + this.i + "," + this.j + ")";
}

function sortCollisionTuple(a, b) {
  if (a.t < b.t) return -1;
  if (a.t > b.t) return 1;
  return 0;
}

export function CollisionTracker(objects, xmin, xmax, ymin, ymax, cr2Objects, crBorder) {
  this.objects = objects;
  this.xmin = xmin;
  this.xmax = xmax;
  this.ymin = ymin;
  this.ymax = ymax;
  this.cr2Objects = cr2Objects;
  this.crBorder = crBorder;
  
  this.nextCollisionList = [];
  
  this.calcExpectedCollisionsForI = function(i, ignoreLowerIndices, skipOther) {
    var ci = objects[i];
    for (var j = 0; j < objects.length; j++) {
      if (j == skipOther) continue;
      
      if (ignoreLowerIndices && j < i) continue;
      
      if (j == i) continue;
      
      var cj = objects[j];
      
      var useI = i;
      var useJ = j;
      var useCi = ci;
      var useCj = cj;
      
      if (j < i) {
        useI = j;
        useJ = i;
        useCi = cj;
        useCj = ci;
      }
      
      var rad2 = (ci.radius + cj.radius) * (ci.radius + cj.radius);
      var t = nextCollision(useCi.x, useCi.y, useCi.vx, useCi.vy, useCi.ax, useCi.ay,
                            useCj.x, useCj.y, useCj.vx, useCj.vy, useCj.ax, useCj.ay,
                            rad2);
      
      if (t >= 0) {
        this.nextCollisionList.push(new CollisionTuple(t, useI, useJ));
      }
    } // end for(j)
    
    // also consider the four borders
    var borderts = nextCollisionWithBorders(ci.x, ci.y, ci.vx, ci.vy, ci.ax, ci.ay, ci.radius, 
                                            xmin, xmax, ymin, ymax, skipOther - objects.length);
    for (var b = 0; b < 4; b++) {
      var t = borderts[b];
      if (t >= 0) {
        this.nextCollisionList.push(new CollisionTuple(t, i, objects.length + b)); 
      }
    }
  }
  
  this.initialize = function() {
    var n = objects.length;
    
    for (var i = 0; i < n; i++) {
      objects[i].updateAcceleration();
    }
    
    for (var i = 0; i < n; i++) {
      this.calcExpectedCollisionsForI(i, true, -1);
    }
    
    // sort the next collision list
    this.nextCollisionList.sort(sortCollisionTuple);
    
    //console.log("initialize");
    //for (var i = 0; i < this.nextCollisionList.length; i++) {
    //  console.log(this.nextCollisionList[i]);
    //}
  }
  
  this.recalculateNextCollision = function() {
    if (this.nextCollisionList.length == 0) return null;
    
    var next = this.nextCollisionList.shift();
    var ci = objects[next.i];
    
    // update position, velocity
    ci.x = getPosition(ci.x, ci.vx, ci.ax, next.t);
    ci.y = getPosition(ci.y, ci.vy, ci.ay, next.t);
    ci.vx = getVelocity(ci.vx, ci.ax, next.t);
    ci.vy = getVelocity(ci.vy, ci.ay, next.t);
    
    if (next.j < objects.length) { // object-object collision
      var cj = objects[next.j];
      
      // update position, velocity
      cj.x = getPosition(cj.x, cj.vx, cj.ax, next.t);
      cj.y = getPosition(cj.y, cj.vy, cj.ay, next.t);
      cj.vx = getVelocity(cj.vx, cj.ax, next.t);
      cj.vy = getVelocity(cj.vy, cj.ay, next.t);
      
      var vf = collide(this.cr2Objects, ci.mass, ci.x, ci.y, ci.vx, ci.vy,
                       cj.mass, cj.x, cj.y, cj.vx, cj.vy);
      ci.vx = vf[0];
      ci.vy = vf[1];
      cj.vx = vf[2];
      cj.vy = vf[3];
      
      // update acceleration based on post-collision velocity
      cj.updateAcceleration();
    } else { // object-border collision
      var b = "top";
      if (next.j == objects.length + 1) b = "bottom";
      if (next.j == objects.length + 2) b = "left";
      if (next.j == objects.length + 3) b = "right";
      
      var vf = collideWithBorder(this.crBorder, b, ci.vx, ci.vy);
      ci.vx = vf[0];
      ci.vy = vf[1];
    }
    
    // update acceleration based on post-collision velocity
    ci.updateAcceleration();
    
    // remove entries from nextCollisionList that with one of the objects from this collision
    this.nextCollisionList = this.nextCollisionList.filter(t => (t.i != next.i && t.j != next.i && (next.j >= objects.length || (t.i != next.j && t.j != next.j))));
    
    // update the times in nextCollisionList
    for (var i = 0; i < this.nextCollisionList.length; i++) {
      this.nextCollisionList[i].t -= next.t;
    }
    
    // now update all the other objects' positions, velocities, and accelerations
    for (var i = 0; i < objects.length; i++) {
      if (i == next.i || i == next.j) continue;
      objects[i].fastForwardByT(next.t);
    }
    
    // now recalculate expected collision times using updated values
    this.calcExpectedCollisionsForI(next.i, false, next.j);
    if (next.j < objects.length) {
      this.calcExpectedCollisionsForI(next.j, false, next.i);
    }
    // sort the next collision list
    this.nextCollisionList.sort(sortCollisionTuple);
  }
  
  // run forward until tmax is reached
  this.runToTime = function(tmax) {
    var simT = 0.0;
    while (this.nextCollisionList.length > 0 && simT + this.nextCollisionList[0].t <= tmax) {
      simT += this.nextCollisionList[0].t;
      
      this.recalculateNextCollision();
    }
    
    // update positions to the desired time
    for (var i = 0; i < objects.length; i++) {
      objects[i].fastForwardByT(tmax - simT);
    }
    
    // update expected collision times
    for (var i = 0; i < this.nextCollisionList.length; i++) {
      this.nextCollisionList[i].t -= (tmax - simT);
    }
  }
}