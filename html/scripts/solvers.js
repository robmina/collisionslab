
// translated from https://github.com/fpsunflower/sunflow

export function solveQuadric(a, b, c) {
  if (a == 0) return null;
  
  var disc = b * b - 4 * a * c;
  if (disc < 0) {
    return null;
  }
  disc = Math.sqrt(disc);
  var q = ((b < 0) ? -0.5 * (b - disc) : -0.5 * (b + disc));
  var t0 = q / a;
  var t1 = c / q;
  // return sorted array
  return (t0 > t1) ? [t1, t0] : [t0, t1];
}

export function solveQuartic(a, b, c, d, e) {
    if (a == 0) return null; // added by robmina to avoid division by zero

    var inva = 1 / a;
    var c1 = b * inva;
    var c2 = c * inva;
    var c3 = d * inva;
    var c4 = e * inva;
    // cubic resolvant
    var c12 = c1 * c1;
    var p = -0.375 * c12 + c2;
    var q = 0.125 * c12 * c1 - 0.5 * c1 * c2 + c3;
    var r = -0.01171875 * c12 * c12 + 0.0625 * c12 * c2 - 0.25 * c1 * c3 + c4;
    var z = solveCubicForQuartic(-0.5 * p, -r, 0.5 * r * p - 0.125 * q * q);
    var d1 = 2.0 * z - p;
    if (d1 < 0) {
      if (d1 > -1.0e-4)
        d1 = 0;
      else
        return null;
    }
    var d2;
    if (d1 < 1.0e-8) {
      d2 = z * z - r;
      if (d2 < 0)
        return null;
      d2 = Math.sqrt(d2);
    } else {
      d1 = Math.sqrt(d1);
      d2 = 0.5 * q / d1;
    }
    // setup usefull values for the quadratic factors
    var q1 = d1 * d1;
    var q2 = -0.25 * c1;
    var pm = q1 - 4 * (z - d2);
    var pp = q1 - 4 * (z + d2);
    if (pm >= 0 && pp >= 0) {
      // 4 roots (!)
      pm = Math.sqrt(pm);
      pp = Math.sqrt(pp);
      var results = [0.0, 0.0, 0.0, 0.0];
      results[0] = -0.5 * (d1 + pm) + q2;
      results[1] = -0.5 * (d1 - pm) + q2;
      results[2] = 0.5 * (d1 + pp) + q2;
      results[3] = 0.5 * (d1 - pp) + q2;
      // tiny insertion sort
      for (var i = 1; i < 4; i++) {
        for (var j = i; j > 0 && results[j - 1] > results[j]; j--) {
          var t = results[j];
          results[j] = results[j - 1];
          results[j - 1] = t;
        }
      }
      return results;
    } else if (pm >= 0) {
      pm = Math.sqrt(pm);
      var results = [0.0, 0.0];
      results[0] = -0.5 * (d1 + pm) + q2;
      results[1] = -0.5 * (d1 - pm) + q2;
      return results;
    } else if (pp >= 0) {
      pp = Math.sqrt(pp);
      var results = [0.0, 0.0];
      results[0] = 0.5 * (d1 - pp) + q2;
      results[1] = 0.5 * (d1 + pp) + q2;
      return results;
    }
    return null;
}

function solveCubicForQuartic(p, q, r) {
    var A2 = p * p;
    var Q = (A2 - 3.0 * q) / 9.0;
    var R = (p * (A2 - 4.5 * q) + 13.5 * r) / 27.0;
    var Q3 = Q * Q * Q;
    var R2 = R * R;
    var d = Q3 - R2;
    var an = p / 3.0;
    if (d >= 0) {
      d = R / Math.sqrt(Q3);
      var theta = Math.acos(d) / 3.0;
      var sQ = -2.0 * Math.sqrt(Q);
      return sQ * Math.cos(theta) - an;
    } else {
      var sQ = Math.pow(Math.sqrt(R2 - Q3) + Math.abs(R), 1.0 / 3.0);
      if (R < 0)
        return (sQ + Q / sQ) - an;
      else
        return -(sQ + Q / sQ) - an;
    }
}