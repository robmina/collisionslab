
import { solveQuadric, solveQuartic } from './solvers.js';

// the order is top, bottom, left, right
export function nextCollisionWithBorders(x0, y0, vx0, vy0, ax, ay,
                                         r, xmin, xmax, ymin, ymax,
                                         skipIndex) {
    var ttop = -1;
    var tbot = -1;
    var tleft = -1;
    var tright = -1;
    
    if (ax == 0) { // no need to solve quadratic
      tleft = (xmin - x0 + r) / vx0;
      if (tleft < 0 || !isFinite(tleft)) tleft = -1;
      
      tright = (xmax - x0 - r) / vx0;
      if (tright < 0 || !isFinite(tright)) tright = -1;
    } else {
      var tmax = -1 * vx0 / ax;
      
      var pots = solveQuadric(0.5 * ax, vx0, x0 - r - xmin);
      if (pots != null && pots.length > 0 && pots[0] > 0 && pots[0] < tmax) {
        tleft = pots[0];
      } else if (pots != null && pots.length > 1 && pots[1] > 0 && pots[1] < tmax) {
        tleft = pots[1];
      }
      
      pots = solveQuadric(0.5 * ax, vx0, x0 + r - xmax);
      if (pots != null && pots.length > 0 && pots[0] > 0 && pots[0] < tmax) {
        tright = pots[0];
      } else if (pots != null && pots.length > 1 && pots[1] > 0 && pots[1] < tmax) {
        tright = pots[1];
      }
    }
    
    if (ay == 0) { // no need to solve quadratic
      ttop =  (ymax - y0 - r) / vy0;
      if (ttop < 0 || !isFinite(ttop)) ttop = -1;
      
      tbot = (ymin - y0 + r) / vy0;
      if (tbot < 0 || !isFinite(tbot)) tbot = -1;
    } else {
      var tmax = -1 * vy0 / ay;
      
      var pots = solveQuadric(0.5 * ay, vy0, y0 + r - ymax);
      if (pots != null && pots.length > 0 && pots[0] > 0 && pots[0] < tmax) {
        ttop = pots[0];
      } else if (pots != null && pots.length > 1 && pots[1] > 0 && pots[1] < tmax) {
        ttop = pots[1];
      }
      
      pots = solveQuadric(0.5 * ay, vy0, y0 - r - ymin);
      if (pots != null && pots.length > 0 && pots[0] > 0 && pots[0] < tmax) {
        tbot = pots[0];
      } else if (pots != null && pots.length > 1 && pots[1] > 0 && pots[1] < tmax) {
        tbot = pots[1];
      }
    }
    
    // apply the skipIndex:
    if (skipIndex == 0) ttop = -1;
    if (skipIndex == 1) tbot = -1;
    if (skipIndex == 2) tleft = -1;
    if (skipIndex == 3) tright = -1;
    
    return [ ttop, tbot, tleft, tright ];
}

function nextCollisionFull(x10, y10, v1x, v1y, a1x, a1y,
                                  x20, y20, v2x, v2y, a2x, a2y,
                                  rad2, delt) {
  var dx0 = x20 - x10;
  var dy0 = y20 - y10;
  var d02 = dx0 * dx0 + dy0 * dy0;
  if (d02 < rad2) {
    return -1; // circles overlap at t=0
  }
  
  var dvx = v2x - v1x;
  var dvy = v2y - v1y;
  var dv2 = dvx * dvx + dvy * dvy;
  
  var dax = a2x - a1x;
  var day = a2y - a1y;
  var da2 = dax * dax + day * day;
  
  var a = da2 / 4;
  var b = dvx * dax + dvy * day;
  var c = dv2 + dx0 * dax + dy0 * day;
  var d = 2 * (dx0 * dvx + dy0 * dvy);
  var e = d02 - rad2;

  var ts = solveQuartic(a, b, c, d, e);
  
  if (ts == null) return -1;
  if (ts.length > 0 && ts[0] > 0 && ts[0] < delt) return ts[0];
  if (ts.length > 1 && ts[1] > 0 && ts[1] < delt) return ts[1];
  if (ts.length > 2 && ts[2] > 0 && ts[2] < delt) return ts[2];
  if (ts.length > 3 && ts[3] > 0 && ts[3] < delt) return ts[3];
  return -1;
}

function nextCollisionOneStopped(xs, ys, x0, y0, vx, vy, ax, ay, rad2, delt) {
  var dx0 = x0 - xs;
  var dy0 = y0 - ys;
  var d02 = dx0 * dx0 + dy0 * dy0;
  if (d02 < rad2) {
    return -1; // circles overlap at t=0
  }
  
  var v02 = vx * vx + vy * vy;
  
  var a2 = ax * ax + ay * ay;
  
  var a = a2 / 4;
  var b = vx * ax + vy * ay;
  var c = v02 + dx0 * ax + dy0 * ay;
  var d = 2 * (dx0 * vx + dy0 * vy);
  var e = d02 - rad2;
    
  // special case we need to check for
  var ts = [];
  if (a == 0 && b == 0) {
    ts = solveQuadric(c, d, e);
  } else {
    ts = solveQuartic(a, b, c, d, e);
  }
  
  if (ts == null) return -1;
  if (ts.length > 0 && ts[0] > 0 && ts[0] < delt) return ts[0];
  if (ts.length > 1 && ts[1] > 0 && ts[1] < delt) return ts[1];
  if (ts.length > 2 && ts[2] > 0 && ts[2] < delt) return ts[2];
  if (ts.length > 3 && ts[3] > 0 && ts[3] < delt) return ts[3];
  return -1;
}

function nextCollisionNoAcc(x10, y10, v1x, v1y,
                            x20, y20, v2x, v2y,
                            rad2) {
  var dx0 = x20 - x10;
  var dy0 = y20 - y10;
  var d02 = dx0 * dx0 + dy0 * dy0;
  
  var dvx = v2x - v1x;
  var dvy = v2y - v1y;
  var dv2 = dvx * dvx + dvy * dvy;
  
  var a = dv2;
  var b = 2 * (dx0 * dvx + dy0 * dvy);
  var c = d02 - rad2;
  
  var ts = solveQuadric(a, b, c);
  if (ts == null) return -1;
  if (ts.length > 0 && ts[0] > 0) return ts[0];
  if (ts.length > 1 && ts[1] > 0) return ts[1];
  return -1;
}

export function getPosition(x0, v0, a, t) {
  var tmax = Number.MAX_VALUE;
  if (a != 0) {
    tmax = -1 * v0 / a;
  }
  
  if (t > tmax) {
    return x0 + v0 * tmax + 0.5 * a * tmax * tmax;
  } else {
    return x0 + v0 * t + 0.5 * a * t * t;
  }
}

export function getVelocity(v0, a, t) {
  var tmax = Number.MAX_VALUE;
  if (a != 0) {
    tmax = -1 * v0 / a;
  }
  
  if (t > tmax) {
    return 0.0;
  } else {
    return v0 + a * t;
  }
}

export function nextCollision(x10, y10, v1x, v1y, a1x, a1y,
                              x20, y20, v2x, v2y, a2x, a2y,
                              rad2) {
  // Extra check to handle spurious collisions when objects are very nearly touching
  // Check that the angle between the displacement vector and the relative velocity
  // is less than pi/2. Otherwise, the objects are moving away from each other
  // The dot product should be negative.
  // NB: this only works when acceleration and velocity are collinear
  var dotRAndVrel = (x20 - x10) * (v2x - v1x) + (y20 - y10) * (v2y - v1y);
  if (dotRAndVrel > 0) return -1;
                                
  // Determine which case we are in
  if ((v1x != 0 || v1y != 0) && (v2x != 0 || v2y != 0)) {
    //  Case A
    var t1 = 0.0;
    if (a1x != 0.0) {
      t1 = -1 * v1x / a1x;
    } else if (a1y != 0.0) {
      t1 = -1 * v1y / a1y;
    } else {
      t1 = Number.MAX_VALUE;
    }
    
    var t2 = 0.0;
    if (a2x != 0.0) {
      t2 = -1 * v2x / a2x;
    } else if (a2y != 0.0) {
      t2 = -1 * v2y / a2y;
    } else {
      t2 = Number.MAX_VALUE;
    }
    
    if (t1 == Number.MAX_VALUE && t2 == Number.MAX_VALUE) {
      // this means that a1 = 0 and a2 = 0
      // we can solve using a quadratic, rather than a quartic
      return nextCollisionNoAcc(x10, y10, v1x, v1y, x20, y20, v2x, v2y, rad2);
    }
    
    var nextT = nextCollisionFull(x10, y10, v1x, v1y, a1x, a1y,
                                  x20, y20, v2x, v2y, a2x, a2y,
                                  rad2, Math.min(t1, t2));
                                  
    if (nextT > 0) return nextT;
    
    if (t2 < t1) {
      // Case B, 2 stops first
      nextT = t2 + nextCollisionOneStopped(getPosition(x20, v2x, a2x, t2), getPosition(y20, v2y, a2y, t2),
                                           getPosition(x10, v1x, a1x, t2), getPosition(y10, v1y, a1y, t2),
                                           getVelocity(v1x, a1x, t2), getVelocity(v1y, a1y, t2),
                                           a1x, a1y,
                                           rad2, t1 - t2);
      if (nextT > 0) return nextT;
      return -1; // Case D
    } else if (t1 < t2) {
      nextT = t1 + nextCollisionOneStopped(getPosition(x10, v1x, a1x, t1), getPosition(y10, v1y, a1y, t1),
                                           getPosition(x20, v2x, a2x, t1), getPosition(y20, v2y, a2y, t1),
                                           getVelocity(v2x, a2x, t1), getVelocity(v2y, a2y, t1),
                                           a2x, a2y,
                                           rad2, t2 - t1);
      if (nextT > 0) return nextT;
      return -1; // Case D
    } else { // implies t1 == t2
      return -1;
    }
  } // end Case A
  
  if (v1x != 0 || v1y != 0) {
    // Case B, 2 is stopped
    var t1 = 0.0;
    if (a1x != 0.0)
      t1 = -v1x / a1x;
    else if (a1y != 0.0)
      t1 = -v1y / a1y;
    else { // implies a1x == 0 and a1y == 0
      // we can solve the problem using a quadratic, rather than a quartic
      return nextCollisionNoAcc(x10, y10, v1x, v1y,
                                x20, y20, v2x, v2y,
                                rad2);
    }
      
    var nextT = nextCollisionOneStopped(x20, y20,
                                       x10, y10, v1x, v1y, a1x, a1y,
                                       rad2, t1);
      
    if (nextT > 0) return nextT;
    return -1.0; // Case D
  }
  
  if (v2x != 0 || v2y != 0) {
    // Case C, 1 is stopped
    var t2 = 0.0;
    if (a2x != 0.0)
      t2 = -v2x / a2x;
    else if (a2y != 0.0)
      t2 = -v2y / a2y;
    else { // implies a2x == 0 and a2y == 0
      // we can solve the problem using a quadratic, rather than a quartic
      return nextCollisionNoAcc(x10, y10, v1x, v1y,
                                x20, y20, v2x, v2y,
                                rad2);
    }
      
    var nextT = nextCollisionOneStopped(x10, y10,
                                        x20, y20, v2x, v2y, a2x, a2y,
                                        rad2, t2);
      
    if (nextT > 0) return nextT;
    return -1.0;
  }
    
  // Case D, both are stopped
  return -1.0;
}