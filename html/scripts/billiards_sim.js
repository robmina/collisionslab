
// 1:2 aspect ratio to match billiards table
const canvasWidth = 1080;
const canvasHeight = 540;

const updateTimeMS = 10;

var viewArea = {
  canvas : document.createElement("canvas"),
  start : function() {
    this.canvas.width = canvasWidth;
    this.canvas.height = canvasHeight;
    this.context = this.canvas.getContext("2d");
    document.body.insertBefore(this.canvas, document.body.childNodes[0]); // I don't know what this does
    this.frameNo = 0;
    this.interval = setInterval(updateViewArea, updateTimeMS);
    
    //this.encoder = new GIFEncoder();
    //this.encoder.setRepeat(0);
    //this.encoder.setDelay(updateTimeMS);
    //this.encoder.start();
  },
  clear : function() {
    this.context.beginPath();
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
}

function TextComponent(font, color, x, y, text) {
  this.font = font;
  this.color = color;
  this.x = x;
  this.y = y;
  this.text = text;
  this.update = function() {
    var ctx = viewArea.context;
    ctx.font = this.font;
    ctx.fillStyle = this.color;
    ctx.fillText(this.text, this.x, this.y);
  }
}

// dimensions of a billiards table (meters)
const xmin = 0.0;
const xmax = 1.98;
const ymin = 0.0;
const ymax = 0.99;

function physicalX2PixelX(x) {
  return (x - xmin) / (xmax - xmin) * canvasWidth;
}

function physicalY2PixelY(y) {
  return (ymax - y) / (ymax - ymin) * canvasHeight;
}

// assume aspect ratio matches, using only x
function physicalRadius2Pixels(radius) {
  return radius / (xmax - xmin) * canvasWidth;
}

function BallComponent(color, mass, radius, x, y, vx, vy, muTimesg) {
  this.color = color;
  this.mass = mass;
  this.radius = radius;
  this.x = x;
  this.y = y; // zero at the bottom, increases upwards
  this.vx = vx;
  this.vy = vy;
  this.ax = 0.0;
  this.ay = 0.0;
  this.muTimesg = muTimesg;
    
  // draw onto the canvas
  this.update = function() {
    var ctx = viewArea.context;
    // change the y coordinate to match the html coordinate system
    ctx.beginPath();
    ctx.arc(physicalX2PixelX(this.x), physicalY2PixelY(this.y), physicalRadius2Pixels(this.radius), 0, 2 * Math.PI)
    ctx.fillStyle = this.color;
    ctx.fill();
  }
  
  // update position, velocity, acceleration using kinematics
  this.fastForwardByT = function(t) {
    // assuming no collisions happen
    var tmax = Number.MAX_VALUE;
    if (this.ax != 0) {
      tmax = -1 * this.vx / this.ax;
    }
    
    if (t > tmax) {
      this.x = this.x + this.vx * tmax + 0.5 * this.ax * tmax * tmax;
      this.vx = 0.0;
      this.ax = 0.0;
      this.y = this.y + this.vy * tmax + 0.5 * this.ay * tmax * tmax;
      this.vy = 0.0;
      this.ay = 0.0;
    } else {
      this.x = this.x + this.vx * t + 0.5 * this.ax * t * t;
      this.vx = this.vx + this.ax * t;
      this.y = this.y + this.vy * t + 0.5 * this.ay * t * t;
      this.vy = this.vy + this.ay * t;
      // ax, ay unchanged
    }
  }
  
  // update acceleration based on velocity
  this.updateAcceleration = function() {
    var vmag2 = this.vx * this.vx + this.vy * this.vy;
    if (vmag2 == 0.0) {
      this.ax = 0.0;
      this.ay = 0.0;
    } else {
      this.ax = -1 * muTimesg * this.vx / Math.sqrt(vmag2);
      this.ay = -1 * muTimesg * this.vy / Math.sqrt(vmag2);
    }
  }
  
  this.updateAcceleration();
}

import { CollisionTracker } from './collision_tracker.js'

var theBalls = [];
var theFrameNo;

// billiards ball mass = 160 g, diameter = 57 mm
const M = 0.16;
const R = 0.0285;
// the cue ball starts 1/4 of the way along the table (x = 0.495 m)
const Y0 = 0.495;
const X0 = 1.485;
const SQRT3 = Math.sqrt(3);
const MUTIMESG = 0.1;
// using a break speed of 5 m/s (rather slow)
theBalls.push(new BallComponent("white", M, R, Y0, Y0, 5.0, 0.0, MUTIMESG));
// the point of the triangle is 3/4 of the way along the table (x = X0 m)
theBalls.push(new BallComponent("yellow", M, R, X0, Y0, 0.0, 0.0, MUTIMESG)); // 1 ball
theBalls.push(new BallComponent("blue", M, R, X0 + SQRT3 * R + 0.001, Y0 + R + 0.001, 0.0, 0.0, MUTIMESG)); // 2 ball, to the right and above
theBalls.push(new BallComponent("orange", M, R, X0 + SQRT3 * R + 0.001, Y0 - R - 0.001, 0.0, 0.0, MUTIMESG)); // 15 ball, to the right and below
theBalls.push(new BallComponent("green", M, R, X0 + 2 * SQRT3 * R + 0.002, Y0 + 2 * R + 0.001, 0.0, 0.0, MUTIMESG)); // 14 ball, third column high
theBalls.push(new BallComponent("black", M, R, X0 + 2 * SQRT3 * R + 0.002, Y0, 0.0, 0.0, MUTIMESG)); // 8 ball, third column mid
theBalls.push(new BallComponent("pink", M, R, X0 + 2 * SQRT3 * R + 0.002, Y0 - 2 * R - 0.001, 0.0, 0.0, MUTIMESG)); // 13 ball, third column low
theBalls.push(new BallComponent("pink", M, R, X0 + 3 * SQRT3 * R + 0.003, Y0 + 3 * R + 0.002, 0.0, 0.0, MUTIMESG)); // 5 ball, fourth column high
theBalls.push(new BallComponent("purple", M, R, X0 + 3 * SQRT3 * R + 0.003, Y0 + R + 0.001, 0.0, 0.0, MUTIMESG)); // 12 ball, fourth column mid-high
theBalls.push(new BallComponent("orange", M, R, X0 + 3 * SQRT3 * R + 0.003, Y0 - R - 0.001, 0.0, 0.0, MUTIMESG)); // 7 ball, fourth column mid-low
theBalls.push(new BallComponent("red", M, R, X0 + 3 * SQRT3 * R + 0.003, Y0 - 3 * R - 0.002, 0.0, 0.0, MUTIMESG)); // 11 ball, fourth column low
theBalls.push(new BallComponent("yellow", M, R, X0 + 4 * SQRT3 * R + 0.004, Y0 + 4 * R + 0.002, 0.0, 0.0, MUTIMESG)); // 9 ball, fifth column high
theBalls.push(new BallComponent("red", M, R, X0 + 4 * SQRT3 * R + 0.004, Y0 + 2 * R + 0.001, 0.0, 0.0, MUTIMESG)); // 3 ball, fifth column mid-high
theBalls.push(new BallComponent("green", M, R, X0 + 4 * SQRT3 * R + 0.004, Y0, 0.0, 0.0, MUTIMESG)); // 6 ball, fifth column mid
theBalls.push(new BallComponent("blue", M, R, X0 + 4 * SQRT3 * R + 0.004, Y0 - 2 * R - 0.001, 0.0, 0.0, MUTIMESG)); // 10 ball, fifth column mid-low
theBalls.push(new BallComponent("purple", M, R, X0 + 4 * SQRT3 * R + 0.004, Y0 - 4 * R - 0.002, 0.0, 0.0, MUTIMESG)); // 4 ball, fifth column low

theFrameNo = new TextComponent("20px Consolas", "black", physicalX2PixelX(xmin + (xmax - xmin) * 0.25), physicalY2PixelY(ymax - 0.1 * (ymax - ymin)), "Frame number: ");

const cr2Objects = 0.8;
const crBorder = 0.8;

var ct = new CollisionTracker(theBalls, xmin, xmax, ymin, ymax, cr2Objects, crBorder);
ct.initialize();

function updateViewArea() {
  viewArea.clear();
  viewArea.frameNo += 1;
  
  ct.runToTime(updateTimeMS / 1000.0); // updates positions of the balls
  for (var i = 0; i < theBalls.length; i++) {
  //  theBalls[i].fastForwardByT(updateTimeMS / 1000.0);
    theBalls[i].update();
  }
  theFrameNo.text = "Frame number: " + viewArea.frameNo;
  theFrameNo.update();
  
  //if (viewArea.frameNo < 500)
  //  viewArea.encoder.addFrame(viewArea.context);
  //if (viewArea.frameNo == 500) {
  //  viewArea.encoder.finish();
  //  viewArea.encoder.download("download.gif");
  //}
}

// main
viewArea.start();


