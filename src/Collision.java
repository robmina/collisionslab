
/**
 * A utility class with functions for calculating final velocities after collisions.
 */
public class Collision {
  
  /**
   * Find the velocity of an object after colliding with a border, and update the CircleMass velocity appropriately.
   * @param cr The coefficient of restitution
   * @param b Which border the object is colliding with
   * @param a The object that is colliding with a border -- the velocity will be updated
   * @return { vx, vy } after the collision, or null if unphysical value of cr was given
   */
  public static double[] collideWithBorder(double cr, Border b, CircleMass a) {
    double[] res = collideWithBorder(cr, b, a.getVx(), a.getVy());
    
    if (res == null) return null; // improper input
    
    a.getVel().x = res[0];
    a.getVel().y = res[1];
    
    return res;
  }
  
  /**
   * Find the velocity of an object after colliding with a border.
   * @param cr The coefficient of restitution
   * @param b Which border the object is colliding with
   * @param vx The initial x component of velocity
   * @param vy The initial y component of velocity
   * @return { vx, vy } after the collision, or null if unphysical value of cr was given
   */
  public static double[] collideWithBorder(double cr, Border b, double vx, double vy) {
    if (cr < 0 || cr > 1) {
      return null; // these are meaningless values
    }
    
    switch(b) {
      case TOP:
      case BOTTOM:
        return new double[] { vx, -cr * vy };
      case LEFT:
      case RIGHT:
        return new double[] { -cr * vx, vy };
      default:
        return null;
    }
  }
  
  /**
   * Find the velocities of the two objects after the collision, and update the CircleMass velocities appropriately.
   * This assumes no surface friction between the objects.
   * @param cr The coefficient of restitution
   * @param a object 1 -- the velocity will be updated according to the calculation
   * @param b object 2 -- the velocity will be updated according to the calculation
   * @return { vax, vay, vbx, vby } after the collision, or null if no meaningful collision occurs
   */
  public static double[] collide(double cr, CircleMass a, CircleMass b) {
    double[] res = collide(cr, 
                           a.getM(), a.getX(), a.getY(), a.getVx(), a.getVy(),
                           b.getM(), b.getX(), b.getY(), b.getVx(), b.getVy());
    
    if (res == null) return null; // improper input
    
    a.getVel().x = res[0];
    a.getVel().y = res[1];
    b.getVel().x = res[2];
    b.getVel().y = res[3];
    
    return res;
  }
  
  /**
   * Find the velocities of the two objects after the collision.
   * This assumes no surface friction between the objects.
   * @param cr The coefficient of restitution
   * @param m1 mass of object 1
   * @param x1 x coordinate of object 1
   * @param y1 y coordinate of object 1
   * @param v1x x component of velocity of object 1
   * @param v1y y component of velocity of object 1
   * @param m2 mass of object 2
   * @param x2 x coordinate of object 2
   * @param y2 y coordinate of object 2
   * @param v2x x component of velocity of object 2
   * @param v2y y component of velocity of object 2
   * @return { v1x, v1y, v2x, v2y } after the collision, or null if no meaningful collision occurs
   */
  public static double[] collide(double cr,
                                 double m1, double x1, double y1, double v1x, double v1y,
                                 double m2, double x2, double y2, double v2x, double v2y) {
    if (cr < 0 || cr > 1) {
      return null; // these are meaningless values
    }
    
    if (m1 == 0 && m2 == 0) {
      return null; // meaningless
    }
    
    if (m1 == 0 || m2 == 0) { // no collision
      return new double[] { v1x, v1y, v2x, v2y };
    }
    
    double vxcm = (m1 * v1x + m2 * v2x) / (m1 + m2);
    double vycm = (m1 * v1y + m2 * v2y) / (m1 + m2);
    
    // boost into CM frame
    v1x -= vxcm;
    v1y -= vycm;
    
    double dx = x2 - x1;
    double dy = y2 - y1;
    double d2 = dx * dx + dy * dy;
    
    double v1xf = (v1x * (dy * dy - cr * dx * dx) - v1y * (1 + cr) * dx * dy) / d2;
    double v1yf = -1 * (v1x * (1 + cr) * dx * dy - v1y * (dx * dx - cr * dy * dy)) / d2;
    
    v2x = -1 * (m1 / m2) * v1xf + vxcm; // boost back into lab frame
    v2y = -1 * (m1 / m2) * v1yf + vycm;
    
    // boost back into lab frame
    return new double[] { v1xf + vxcm, v1yf + vycm, v2x, v2y };
  }
  
}
