
/**
 * A circular object capable of colliding with other objects.
 * It has mass, radius, position, velocity, and acceleration.
 */
public class CircleMass {
  private Vec2 pos;
  private Vec2 vel;
  private Vec2 acc;
  private double m;
  private double r;
  
  public CircleMass(Vec2 pos, Vec2 vel, Vec2 acc, double m, double r) {
    this.pos = pos;
    this.vel = vel;
    this.acc = acc;
    this.m = m;
    this.r = r;
  }
  
  public CircleMass(double x, double y, double vx, double vy, 
                    double ax, double ay, double m, double r) {
    this.pos = new Vec2(x, y);
    this.vel = new Vec2(vx, vy);
    this.acc = new Vec2(ax, ay);
    this.m = m;
    this.r = r;
  }
  
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof CircleMass)) return false;
    
    CircleMass other = (CircleMass) obj;
    return (other.pos.equals(pos) && 
            other.vel.equals(vel) && 
            other.acc.equals(acc) &&
            other.m == m          &&
            other.r == r);
  }

  @Override
  public CircleMass clone() {
    return new CircleMass(pos.clone(), vel.clone(), acc.clone(), m, r);
  }

  @Override
  public String toString() {
    return "Circle mass " + m + " radius " + r + " position " + pos 
        + " velocity " + vel + " acceleration " + acc;
  }

  public double getX() {
    return pos.x;
  }
  
  public double getY() {
    return pos.y;
  }
  
  public double getVx() {
    return vel.x;
  }
  
  public double getVy() {
    return vel.y;
  }
  
  public double getAx() {
    return acc.x;
  }
  
  public double getAy() {
    return acc.y;
  }
  
  public Vec2 getPos() {
    return pos;
  }
  
  public Vec2 getVel() {
    return vel;
  }
  
  public Vec2 getAcc() {
    return acc;
  }
  
  public double getM() {
    return m;
  }
  
  public double getR() {
    return r;
  }
}
