import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 * @author Rob Mina (rob.mina@charchrist.com)
 * 
 * CollisionTrackerTextOutput.java
 * Jul 16, 2020
 */

/**
 * Handles simulation output by storing positions (only), then outputting them to a csv file at the end of the simulation.
 */
public class CollisionTrackerTextOutput implements CollisionTrackerOutput {

  /**
   * The stored positions.
   */
  private ArrayList<ArrayList<Vec2>> positions;
  /**
   * The output filename.
   */
  private String outputFile;
  
  /**
   * @param outputFile the output filename
   */
  public CollisionTrackerTextOutput(String outputFile) {
    positions = new ArrayList<>();
    
    this.outputFile = outputFile;
  }
  
  /**
   * Does nothing
   */
  @Override
  public void initializeForOutput(CollisionTracker t) {
    // nothing to do
  }

  /**
   * Instantiates empty ArrayLists.
   */
  @Override
  public void handleOutput(CollisionTracker t) {
    positions.add(new ArrayList<Vec2>());
    for (CircleMass c : t.getObjects()) {
      positions.get(positions.size() - 1).add(c.getPos().clone());
    }
  }

  /**
   * @param fname path to the output file
   * @param str string to write into the file
   */
  public static void printToFile(String fname, String str) {
    try (PrintStream out = new PrintStream(new FileOutputStream(fname))) {
      out.print(str);
    } catch (FileNotFoundException e) {
      System.out.println("Warning: could not write file " + fname);
    }
  }
  
  /**
   * Output all of the stored positions in a csv file.
   */
  @Override
  public void finalizeOutput(CollisionTracker t) {
    if (positions.size() == 0) return;
    
    int n = positions.get(0).size();
    String outcsv = "time,";
    
    for (int i = 0; i < n; i++) {
      outcsv += "x" + i + ",y" + i + ",";
    }
    // remove trailing comma, add newline
    outcsv = outcsv.substring(0, outcsv.length() - 1) + "\n";

    for (ArrayList<Vec2> posVec : positions) {
      outcsv += t.getSimulatedTime() + ",";

      for (Vec2 pos : posVec) {
        outcsv += pos.x + "," + pos.y + ",";
      }
      
      // remove trailing comma, add newline
      outcsv = outcsv.substring(0, outcsv.length() - 1) + "\n";
    }
    
    printToFile(outputFile, outcsv);
  }

}
