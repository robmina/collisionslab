
/**
 * A 2D vector class with components stored as doubles.
 * The components are mutable, for convenience.
 */
public class Vec2 {
  public double x;
  public double y;
  
  public Vec2(double x, double y) {
    this.x = x;
    this.y = y;
  }
  
  /**
   * @return the length of the vector
   */
  public double magnitude() {
    return Math.sqrt(mag2());
  }
  
  /**
   * @return the magnitude, squared
   */
  public double mag2() {
    return x * x + y * y;
  }
  
  /**
   * Rotate this vector clockwise by the specified amount
   * @param theta angle (radians) by which to rotate (use negative value for CCW rotation)
   */
  public void rotateCW(double theta) {
    double newx = Math.cos(theta) * x + Math.sin(theta) * y;
    y = Math.cos(theta) * y - Math.sin(theta) * x;
    x = newx;
  }
  
  /**
   * This creates a new vector. To modify an existing vector, use scale(-1)
   * @return a new vector in the opposite direction of this one
   */
  public Vec2 neg() {
    return new Vec2(-x, -y);
  }
  
  /**
   * @return a new vector in the same direction as this one, with length 1
   */
  public Vec2 unit() {
    double m = magnitude();
    return new Vec2(x / m, y / m);
  }
  
  /**
   * Scale this vector by the factor d
   * @param d the scale factor
   */
  public void scale(double d) {
    x *= d;
    y *= d;
  }
  
  /**
   * Project this vector onto another (https://en.wikipedia.org/wiki/Vector_projection)
   * @param other the vector to project onto
   * @return a new vector, the vector projection
   */
  public Vec2 parallelComp(Vec2 other) {
    Vec2 ret = other.unit();
    ret.scale(dot(this, ret));
    return ret;
  }
  
  /**
   * Find the vector rejection this from another (https://en.wikipedia.org/wiki/Vector_projection)
   * @param other other vector to find the perp component to
   * @return a new vector, the vector rejection
   */
  public Vec2 perpendicularComp(Vec2 other) {
    return difference(this, parallelComp(other));
  }

  @Override
  public boolean equals(Object obj) {
    return (obj instanceof Vec2 && ((Vec2) obj).x == x && ((Vec2) obj).y == y);
  }

  @Override
  public String toString() {
    return "<" + x + "," + y + ">";
  }
  
  /**
   * @param a first vector
   * @param b second vector
   * @return the dot/scalar product
   */
  public static double dot(Vec2 a, Vec2 b) {
    return a.x * b.x + a.y * b.y;
  }
  
  /**
   * @param a first vector
   * @param b second vector
   * @return the vector sum
   */
  public static Vec2 sum(Vec2 a, Vec2 b) {
    return new Vec2(a.x + b.x, a.y + b.y);
  }
  
  /**
   * @param a first vector
   * @param b second vector
   * @return a - b (vector difference)
   */
  public static Vec2 difference(Vec2 a, Vec2 b) {
    return sum(a, b.neg());
  }

  @Override
  public Vec2 clone() {
    return new Vec2(x, y);
  }
}
