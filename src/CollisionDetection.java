
/**
 * A utility class with functions for calculating the time of future collisions.
 */
public final class CollisionDetection {
  
  /**
   * Find the times of the next collisions with each of the four borders.
   * @param a the circular object
   * @param xmin the x position of the left border
   * @param xmax the x position of the right border
   * @param ymin the y position of the bottom border
   * @param ymax the y position of the top border
   * @return { ttop, tbot, tleft, tright } or -1 if no collision expected
   */
  public static double[] nextCollisionWithBorders(CircleMass a, double xmin, double xmax, double ymin, double ymax) {
    return nextCollisionWithBorders(a.getX(), a.getY(), a.getVx(), a.getVy(), a.getAx(), a.getAy(),
                                    a.getR(), xmin, xmax, ymin, ymax);
  }
  
  /**
   * Find the times of the next collisions with each of the four borders.
   * @param x0 the initial x coordinate
   * @param y0 the initial y coordinate
   * @param vx0 the initial x component of velocity
   * @param vy0 the initial y component of velocity
   * @param ax the initial x component of acceleration
   * @param ay the initial y component of acceleration
   * @param xmin the x position of the left border
   * @param xmax the x position of the right border
   * @param ymin the y position of the bottom border
   * @param ymax the y position of the top border
   * @return { ttop, tbot, tleft, tright } or -1 if no collision expected
   */
  public static double[] nextCollisionWithBorders(double x0, double y0, double vx0, double vy0, double ax, double ay,
                                                  double r, double xmin, double xmax, double ymin, double ymax) {
    double ttop = -1;
    double tbot = -1;
    double tleft = -1;
    double tright = -1;
    
    if (ax == 0) { // no need to solve quadratic
      tleft = (xmin - x0 + r) / vx0;
      if (tleft < 0) tleft = -1;
      
      tright = (xmax - x0 - r) / vx0;
      if (tright < 0) tright = -1;
    } else {
      double tmax = -vx0 / ax;
      
      double[] pots = Solvers.solveQuadric(0.5 * ax, vx0, x0 - r - xmin);
      if (pots != null && pots.length > 0 && pots[0] > 0 && pots[0] < tmax) 
        tleft = pots[0];
      else if (pots != null && pots.length > 1 && pots[1] > 0 && pots[1] < tmax) 
        tleft = pots[1];
      
      pots = Solvers.solveQuadric(0.5 * ax, vx0, x0 + r - xmax);
      if (pots != null && pots.length > 0 && pots[0] > 0 && pots[0] < tmax) 
        tright = pots[0];
      else if (pots != null && pots.length > 1 && pots[1] > 0 && pots[1] < tmax) 
        tright = pots[1];
    }
    
    if (ay == 0) { 
      ttop = (ymax - y0 - r) / vy0;
      if (ttop < 0) ttop = -1;
      
      tbot = (ymin - y0 + r) / vy0;
      if (tbot < 0) tbot = -1;
    } else {
      double tmax = -vy0 / ay;
      
      double[] pots = Solvers.solveQuadric(0.5 * ay, vy0, y0 + r - ymax);
      if (pots != null && pots.length > 0 && pots[0] > 0 && pots[0] < tmax) 
        ttop = pots[0];
      else if (pots != null && pots.length > 1 && pots[1] > 0 && pots[1] < tmax) 
        ttop = pots[1];
      
      pots = Solvers.solveQuadric(0.5 * ay, vy0, y0 - r - ymin);
      if (pots != null && pots.length > 0 && pots[0] > 0 && pots[0] < tmax) 
        tbot = pots[0];
      else if (pots != null && pots.length > 1 && pots[1] > 0 && pots[1] < tmax) 
        tbot = pots[1];
    }
    
    return new double[] { ttop, tbot, tleft, tright };
  }
  
  /**
   * Find the time of the next collision between t=0 and t=delt.
   * This is the full version in which both circles have constant acceleration.
   * @param x10 Initial x coordinate of circle 1
   * @param y10 Initial y coordinate of circle 1
   * @param v1x Initial x component of velocity of circle 1
   * @param v1y Initial y component of velocity of circle 1
   * @param a1x x component of acceleration of circle 1
   * @param a1y y component of acceleration of circle 1
   * @param x20 Initial x coordinate of circle 2
   * @param y20 Initial y coordinate of circle 2
   * @param v2x Initial x component of velocity of circle 2
   * @param v2y Initial y component of velocity of circle 2
   * @param a2x x component of acceleration of circle 2
   * @param a2y y component of acceleration of circle 2
   * @param rad2 (r_1 + r_2)^2 -- sum of radii, squared
   * @param delt upper limit on collision time
   * @return the time of the next collision between t=0 and t=delt, or -1 if no such collision
   */
  public static double nextCollisionFull(double x10, double y10, double v1x, double v1y, double a1x, double a1y,
                                         double x20, double y20, double v2x, double v2y, double a2x, double a2y,
                                         double rad2, double delt) {
    double dx0 = x20 - x10;
    double dy0 = y20 - y10;
    double d02 = dx0 * dx0 + dy0 * dy0;
    if (d02 < rad2) {
      return -1.0; // circles overlap at t=0
    }
    
    double dvx = v2x - v1x;
    double dvy = v2y - v1y;
    double dv2 = dvx * dvx + dvy * dvy;
    
    double dax = a2x - a1x;
    double day = a2y - a1y;
    double da2 = dax * dax + day * day;
    
    double a = da2 / 4;
    double b = dvx * dax + dvy * day;
    double c = dv2 + dx0 * dax + dy0 * day;
    double d = 2 * (dx0 * dvx + dy0 * dvy);
    double e = d02 - rad2;
    
    double[] ts = Solvers.solveQuartic(a, b, c, d, e);
    if (ts == null) return -1.0;
    if (ts.length > 0 && ts[0] > 0 && ts[0] < delt) return ts[0];
    if (ts.length > 1 && ts[1] > 0 && ts[1] < delt) return ts[1];
    if (ts.length > 2 && ts[2] > 0 && ts[2] < delt) return ts[2];
    if (ts.length > 3 && ts[3] > 0 && ts[3] < delt) return ts[3];
    return -1.0;
  }
  
  /**
   * Find the time of the next collision between t=0 and t=delt.
   * In this version, one of the circles is not moving.
   * @param xs x coordinate of the unmoving circle
   * @param ys y coordinate of the unmoving circle
   * @param x0 Initial x coordinate of moving circle
   * @param y0 Initial y coordinate of moving circle
   * @param vx Initial x component of velocity of moving circle
   * @param vy Initial y component of velocity of moving circle
   * @param ax x component of acceleration of moving circle
   * @param ay y component of acceleration of moving circle
   * @param rad2 (r_1 + r_2)^2 -- sum of radii, squared
   * @param delt upper limit on collision time
   * @return the time of the next collision between t=0 and t=delt, or -1 if no such collision
   */
  public static double nextCollisionOneStopped(double xs, double ys,
                                               double x0, double y0, double vx, double vy, double ax, double ay,
                                               double rad2, double delt) {
    double dx0 = x0 - xs;
    double dy0 = y0 - ys;
    double d02 = dx0 * dx0 + dy0 * dy0;
    
    double v02 = vx * vx + vy * vy;
    
    double a2 = ax * ax + ay * ay;
    
    double a = a2 / 4;
    double b = vx * ax + vy * ay;
    double c = v02 + dx0 * ax + dy0 * ay;
    double d = 2 * (dx0 * vx + dy0 * vy);
    double e = d02 - rad2;
    
    double[] ts;
    // special case we need to check for
    if (a == 0.0 && b == 0.0) {
      ts = Solvers.solveQuadric(c, d, e);
    } else {
      ts = Solvers.solveQuartic(a, b, c, d, e);
    }
    
    if (ts == null) return -1.0;
    if (ts.length > 0 && ts[0] > 0 && ts[0] < delt) return ts[0];
    if (ts.length > 1 && ts[1] > 0 && ts[1] < delt) return ts[1];
    if (ts.length > 2 && ts[2] > 0 && ts[2] < delt) return ts[2];
    if (ts.length > 3 && ts[3] > 0 && ts[3] < delt) return ts[3];
    return -1.0;
  }
  
  /**
   * Find the time (t > 0) of the next collision.
   * This version assumes that both circles move with constant speed.
   * @param x10 Initial x coordinate of circle 1
   * @param y10 Initial y coordinate of circle 1
   * @param v1x x component of velocity of circle 1
   * @param v1y y component of velocity of circle 1
   * @param x20 Initial x coordinate of circle 2
   * @param y20 Initial y coordinate of circle 2
   * @param v2x x component of velocity of circle 2
   * @param v2y y component of velocity of circle 2
   * @param rad2 (r_1 + r_2)^2 -- sum of radii, squared
   * @return the time of the next collision, or -1 if no such collision
   */
  public static double nextCollisionNoAcc(double x10, double y10, double v1x, double v1y,
                                          double x20, double y20, double v2x, double v2y,
                                          double rad2) {
    double dx0 = x20 - x10;
    double dy0 = y20 - y10;
    double d02 = dx0 * dx0 + dy0 * dy0;
    double dvx = v2x - v1x;
    double dvy = v2y - v1y;
    double dv2 = dvx * dvx + dvy * dvy;
    
    double a = dv2;
    double b = 2 * (dx0 * dvx + dy0 * dvy);
    double c = d02 - rad2;
    
    double[] ts = Solvers.solveQuadric(a, b, c);
    if (ts == null) return -1.0;
    if (ts.length > 0 && ts[0] > 0) return ts[0];
    if (ts.length > 1 && ts[1] > 0) return ts[1];
    return -1.0;
  }
  
  /**
   * Find the time (t > 0) of the next collision between circles a and b.
   * It is assumed that the objects move with constant acceleration, and that
   * the acceleration is such that the speed decreases over time.
   * This version is provided for convenience.
   * @param a the first circle
   * @param b the second circle
   * @return the time of the next collision, or -1 if no such collision
   */
  public static double nextCollision(CircleMass a, CircleMass b) {
    double rad2 = (a.getR() + b.getR()) * (a.getR() + b.getR());
    return nextCollision(a.getX(), a.getY(), a.getVx(), a.getVy(), a.getAx(), a.getAy(),
                         b.getX(), b.getY(), b.getVx(), b.getVy(), b.getAx(), b.getAy(),
                         rad2);
  }
  
  public static double getPosition(double x0, double v0, double a, double t) {
    double tmax = Double.MAX_VALUE;
    if (a != 0) {
      tmax = -v0 / a;
    }
    
    if (t > tmax) {
      return x0 + v0 * tmax + 0.5 * a * tmax * tmax;
    }
    
    return x0 + v0 * t + 0.5 * a * t * t;
  }
  
  public static double getVelocity(double v0, double a, double t) {
    double tmax = Double.MAX_VALUE;
    if (a != 0) {
      tmax = -v0 / a;
    }
    
    if (t > tmax) {
      return 0.0;
    }
    
    return v0 + a * t;
  }
  
  public static double getAcceleration(double v0, double a, double t) {
    double tmax = Double.MAX_VALUE;
    if (a != 0) {
      tmax = -v0 / a;
    }
    
    if (t > tmax) {
      return 0.0;
    }
    
    return a;
  }
  
  /**
   * Find the time (t > 0) of the next collision between circles 1 and 2.
   * It is assumed that the objects move with constant acceleration, and that
   * the acceleration is such that the speed decreases over time.
   * @param x10 Initial x coordinate of circle 1
   * @param y10 Initial y coordinate of circle 1
   * @param v1x Initial x component of velocity of circle 1
   * @param v1y Initial y component of velocity of circle 1
   * @param a1x x component of acceleration of circle 1
   * @param a1y y component of acceleration of circle 1
   * @param x20 Initial x coordinate of circle 2
   * @param y20 Initial y coordinate of circle 2
   * @param v2x Initial x component of velocity of circle 2
   * @param v2y Initial y component of velocity of circle 2
   * @param a2x x component of acceleration of circle 2
   * @param a2y y component of acceleration of circle 2
   * @param rad2 (r_1 + r_2)^2 -- sum of radii, squared
   * @return the time of the next collision, or -1 if no such collision
   */
  public static double nextCollision(double x10, double y10, double v1x, double v1y, double a1x, double a1y,
                                     double x20, double y20, double v2x, double v2y, double a2x, double a2y,
                                     double rad2) {
    // Determine which case we are in
    if ((v1x != 0 || v1y != 0) && (v2x != 0 || v2y != 0)) {
      // Case A
      double t1 = 0.0;
      if (a1x != 0.0)
        t1 = -v1x / a1x;
      else if (a1y != 0.0)
        t1 = -v1y / a1y;
      else { // implies a1x == 0 and a1y == 0
        t1 = Double.MAX_VALUE;
      }
      
      double t2 = 0.0;
      if (a2x != 0.0)
        t2 = -v2x / a2x;
      else if (a2y != 0.0)
        t2 = -v2y / a2y;
      else { // implies a2x == 0 and a2y == 0
        t2 = Double.MAX_VALUE;
      }
      
      if (t1 == Double.MAX_VALUE && t2 == Double.MAX_VALUE) {
        // this means that a1 = 0 and a2 = 0
        // we can solve the problem using a quadratic, rather than a quartic
        return nextCollisionNoAcc(x10, y10, v1x, v1y,
                                  x20, y20, v2x, v2y,
                                  rad2);
      }
      
      double nextT = nextCollisionFull(x10, y10, v1x, v1y, a1x, a1y,
                                       x20, y20, v2x, v2y, a2x, a2y,
                                       rad2, Math.min(t1, t2));
      if (nextT > 0) return nextT;
      
      if (t2 < t1) {
        // Case B, 2 stops first
        nextT = t2 + nextCollisionOneStopped(getPosition(x20, v2x, a2x, t2), getPosition(y20, v2y, a2y, t2),
                                             getPosition(x10, v1x, a1x, t2), getPosition(y10, v1y, a1y, t2),
                                             getVelocity(v1x, a1x, t2), getVelocity(v1y, a1y, t2),
                                             a1x, a1y,
                                             rad2, t1 - t2);
        
        if (nextT > 0) return nextT;
        return -1.0; // Case D
      } else if (t1 < t2) {
        // Case C, 1 stops first
        nextT = t1 + nextCollisionOneStopped(getPosition(x10, v1x, a1x, t1), getPosition(y10, v1y, a1y, t1),
                                             getPosition(x20, v2x, a2x, t1), getPosition(y20, v2y, a2y, t1),
                                             getVelocity(v2x, a2x, t1), getVelocity(v2y, a2y, t1),
                                             a2x, a2y,
                                             rad2, t2 - t1);
        
        if (nextT > 0) return nextT;
        return -1.0; // Case D
      } else { // implies t1 == t2
        // Case D
        return -1.0;
      }
    } // end Case A
    
    if (v1x != 0 || v1y != 0) {
      // Case B, 2 is stopped
      double t1 = 0.0;
      if (a1x != 0.0)
        t1 = -v1x / a1x;
      else if (a1y != 0.0)
        t1 = -v1y / a1y;
      else { // implies a1x == 0 and a1y == 0
        // we can solve the problem using a quadratic, rather than a quartic
        return nextCollisionNoAcc(x10, y10, v1x, v1y,
                                  x20, y20, v2x, v2y,
                                  rad2);
      }
      
      double nextT = nextCollisionOneStopped(x20, y20,
                                             x10, y10, v1x, v1y, a1x, a1y,
                                             rad2, t1);
      
      if (nextT > 0) return nextT;
      return -1.0; // Case D
    }
    
    if (v2x != 0 || v2y != 0) {
      // Case C, 1 is stopped
      double t2 = 0.0;
      if (a2x != 0.0)
        t2 = -v2x / a2x;
      else if (a2y != 0.0)
        t2 = -v2y / a2y;
      else { // implies a2x == 0 and a2y == 0
        // we can solve the problem using a quadratic, rather than a quartic
        return nextCollisionNoAcc(x10, y10, v1x, v1y,
                                  x20, y20, v2x, v2y,
                                  rad2);
      }
      
      double nextT = nextCollisionOneStopped(x10, y10,
                                             x20, y20, v2x, v2y, a2x, a2y,
                                             rad2, t2);
      
      if (nextT > 0) return nextT;
      return -1.0;
    }
    
    // Case D, both are stopped
    return -1.0;
  }
  
}
