import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Track collisions of multiple circular masses using exact calculations for positions and velocities at times of collisions.
 * For the assumptions used in deriving the predictions, see https://docs.google.com/document/d/1cg6-m8PooP_YtT_Mzd92pf912XuhdlYwxsztYJnXFvI/edit?usp=sharing
 * Output the positions, velocities, and accelerations at regular time intervals.
 */
public class CollisionTracker {
  
  public static void main(String[] args) {
    CircleMass one = new CircleMass(1.0, 0.0, 1.0, 0.3, 0.0, 0.0, 0.5, 0.12);
    CircleMass two = new CircleMass(2.0, 0.5, -0.5, -0.5, 0.0, 0.0, 1.5, 0.172);
    
    ArrayList<CircleMass> masses = new ArrayList<>();
    masses.add(one);
    masses.add(two);
    
    // these dimensions chosen to match the phet collision lab: https://phet.colorado.edu/sims/collision-lab/collision-lab_en.html
    double xmin = 0.0;
    double xmax = 3.2;
    double ymin = -0.95;
    double ymax = 0.95;
    double cr2Objects = 1.0; // perfectly elastic
    double crBorder = 1.0; // perfectly elastic
    double muTimesg = 0.0; // no friction
    CollisionTracker ct = new CollisionTracker(masses, xmin, xmax, ymin, ymax, cr2Objects, crBorder, muTimesg);
    
    CollisionTrackerTextOutput out = new CollisionTrackerTextOutput("ct_test.csv");
    ct.initialize(out);
    
    double delT = 0.005;
    double tMax = 1.0;
    ct.runSimulation(delT, tMax, out);
  }
  
  /**
   * A simple tuple to store a time and two indices.
   * Sorts in ascending order by time.
   */
  private class NextCollisionTuple implements Comparable<NextCollisionTuple> {
    public double t;
    public int i;
    public int j;
    
    public NextCollisionTuple(double t, int i, int j) {
      this.t = t;
      this.i = i;
      this.j = j;
    }
    
    @Override
    public String toString() {
      return "(" + t + ", " + i + ", " + j + ")";
    }

    @Override
    public int compareTo(NextCollisionTuple o) {
      if (this.t < o.t) return -1;
      if (this.t > o.t) return 1;
      return 0;
    }
  }

  /**
   * A list of all the masses.
   */
  private ArrayList<CircleMass> objects;
  /**
   * The x-coordinate of the left border.
   */
  private double xmin;
  /**
   * The x-coordinate of the right border.
   */
  private double xmax;
  /**
   * The y-coordinate of the bottom border.
   */
  private double ymin;
  /**
   * The y-coordinate of the top border.
   */
  private double ymax;
  /**
   * The coefficient of restitution for circle-circle collisions.
   */
  private double cr2Objects;
  /**
   * The coefficient of restitution for circle-border collisions.
   */
  private double crBorder;
  /**
   * The coefficient of friction times acceleration due to gravity, used in calculating acceleration due to friction.
   */
  private double muTimesg;
  
  /**
   * A table containing the positions and velocities of objects at the time of the next expected collision.
   */
  private ArrayList<Double>[][] expectedCollisionTable;
  /**
   * A sorted set of all expected future collisions.
   */
  private TreeSet<NextCollisionTuple> nextCollisionSet;
  
  /**
   * The elapsed simulated time, in seconds.
   */
  private double simT;
  
  /**
   * @return the number of circles in the simulation.
   */
  public int getN() { return objects.size(); }
  /**
   * The positions, velocities, and accelerations are updated as the simulation runs.
   * @return the list of all the objects, in the same order as passed to the constructor.
   */
  public ArrayList<CircleMass> getObjects() { return objects; }
  /**
   * @return the elapsed simulated time, in seconds
   */
  public double getSimulatedTime() { return simT; }
  
  /**
   * @param objects the list of all the circular objects in the simulation, with their initial positions, velocities, and accelerations.
   * @param xmin the x-coordinate of the left border
   * @param xmax the x-coordinate of the right border
   * @param ymin the y-coordinate of the bottom border
   * @param ymax the y-coordinate of the top border
   * @param cr2Objects the coefficient of restitution for circle-circle collisions
   * @param crBorder the coefficient of restitution for circle-border collisions
   * @param muTimesg the coefficient of friction times acceleration due to gravity, used in calculating acceleration due to friction
   */
  @SuppressWarnings("unchecked")
  public CollisionTracker(ArrayList<CircleMass> objects, double xmin, double xmax, double ymin, double ymax,
                          double cr2Objects, double crBorder, double muTimesg) {
    this.objects = objects;
    this.xmin = xmin;
    this.xmax = xmax;
    this.ymin = ymin;
    this.ymax = ymax;
    this.cr2Objects = cr2Objects;
    this.crBorder = crBorder;
    this.muTimesg = muTimesg;
    
    int n = objects.size();
    this.expectedCollisionTable = (ArrayList<Double>[][]) new ArrayList[n][n+4];
    
    this.nextCollisionSet = new TreeSet<>();
    
    this.simT = 0.0;
  }
  
  /**
   * Calculate the acceleration of the object, based on its current velocity.
   * @param c the object whose acceleration will be updated
   */
  private void updateAcceleration(CircleMass c) {
    double vmag = c.getVel().magnitude();
    if (vmag == 0.0) {
      c.getAcc().x = 0.0;
      c.getAcc().y = 0.0;
    } else {
      c.getAcc().x = -muTimesg * c.getVx() / vmag;
      c.getAcc().y = -muTimesg * c.getVy() / vmag;
    }
  }
  
  /**
   * For testing, print the contents of the expected collision table and the next collision set.
   */
  private void printTables() {
    System.out.println("Expected collision table:");
    
    for (int i = 0; i < objects.size(); i++) {
      System.out.print(" *** ");
      for (int j = 0; j < objects.size() + 4; j++) {
        System.out.print(expectedCollisionTable[i][j] + " ");
      }
      System.out.print("\n");
    }
    
    System.out.println("");
    
    System.out.println("Next collision set:");
    for (NextCollisionTuple t : nextCollisionSet) {
      System.out.println(" *** " + t);
    }
  }
  
  /**
   * Initialize the data structures with expected collision times.
   * This must be called before runSimulation.
   * @param out the output handler on which initializeForOutput will be called
   */
  public void initialize(CollisionTrackerOutput out) {
    int n = objects.size();
    
    // first, calculate acceleration
    for (CircleMass c : objects) {
      updateAcceleration(c);
    }
    
    // next, initialize the collision data structures
    for (int i = 0; i < n; i++) {
      calcExpectedCollisionsForI(i, true, -1);
    }
    
    this.simT = 0.0;
    
    out.initializeForOutput(this);
    
    //printTables();
  }
  
  /**
   * Calculate the expected collision times for one of the objects in the list.
   * @param i the index of the object in the list
   * @param ignoreLowerIndices whether to ignore indices that are smaller than i
   * @param skipOther the index of an object in the list that will be skipped, used immediately after a collision to prevent a second collision
   */
  private void calcExpectedCollisionsForI(int i, boolean ignoreLowerIndices, int skipOther) {
    CircleMass ci = objects.get(i);
    for (int j = 0; j < objects.size(); j++) {
      if (j == skipOther) {
        expectedCollisionTable[i][j] = null;
        continue;
      }
      
      if (ignoreLowerIndices && j < i) {
        expectedCollisionTable[i][j] = null;
        continue;
      }
      
      if (j == i) {
        expectedCollisionTable[i][j] = null;
        continue;
      }
      
      CircleMass cj = objects.get(j);
      
      int useI = i;
      int useJ = j;
      CircleMass useCi = ci;
      CircleMass useCj = cj;
      
      if (j < i) {
        useI = j;
        useJ = i;
        useCi = cj;
        useCj = ci;
      }
      
      double t = CollisionDetection.nextCollision(useCi, useCj);
      
      if (t < 0) expectedCollisionTable[useI][useJ] = null;
      else {
        nextCollisionSet.add(new NextCollisionTuple(t, useI, useJ));

        double xi = CollisionDetection.getPosition(useCi.getX(), useCi.getVx(), useCi.getAx(), t);
        double yi = CollisionDetection.getPosition(useCi.getY(), useCi.getVy(), useCi.getAy(), t);
        double vxi = CollisionDetection.getVelocity(useCi.getVx(), useCi.getAx(), t);
        double vyi = CollisionDetection.getVelocity(useCi.getVy(), useCi.getAy(), t);
        double xj = CollisionDetection.getPosition(useCj.getX(), useCj.getVx(), useCj.getAx(), t);
        double yj = CollisionDetection.getPosition(useCj.getY(), useCj.getVy(), useCj.getAy(), t);
        double vxj = CollisionDetection.getVelocity(useCj.getVx(), useCj.getAx(), t);
        double vyj = CollisionDetection.getVelocity(useCj.getVy(), useCj.getAy(), t);
        expectedCollisionTable[useI][useJ] 
            = new ArrayList<Double>(Arrays.asList(xi, yi, vxi, vyi, xj, yj, vxj, vyj));
      }
    } // end for(j)
    
    // we also need to consider the four borders
    // the order is top, bottom, left, right
    double[] borderts = CollisionDetection.nextCollisionWithBorders(ci, xmin, xmax, ymin, ymax);
    for (int b = 0; b < 4; b++) {
      double t = borderts[b];
      if (t < 0) expectedCollisionTable[i][objects.size() + b] = null;
      else {
        nextCollisionSet.add(new NextCollisionTuple(t, i, objects.size() + b));
        double xi = CollisionDetection.getPosition(ci.getX(), ci.getVx(), ci.getAx(), t);
        double yi = CollisionDetection.getPosition(ci.getY(), ci.getVy(), ci.getAy(), t);
        double vxi = CollisionDetection.getVelocity(ci.getVx(), ci.getAx(), t);
        double vyi = CollisionDetection.getVelocity(ci.getVy(), ci.getAy(), t);
        expectedCollisionTable[i][objects.size() + b] = new ArrayList<Double>(Arrays.asList(xi, yi, vxi, vyi));
      }
    }
  }
  
  // after a collision happens, fast forward by this amount to prevent a second collision
  //private static double SMALL_POST_COLLISION_TIME = 0.00001; // 10 microseconds
  
  /**
   * Recalculate positions, velocities, and accelerations after the next collision.
   * Update the data structures with the expected collisions after this one.
   * This updates the time variable so that t=0 corresponds to the time of the collision.
   * @return the tuple representing the collision that just happened
   */
  private NextCollisionTuple recalculateNextCollision() {
    if (nextCollisionSet.size() == 0) return null; // nothing to do
    
    NextCollisionTuple next = nextCollisionSet.first();
    nextCollisionSet.remove(next);
    CircleMass ci = objects.get(next.i);
    
    // update position, velocity
    ci.getPos().x = expectedCollisionTable[next.i][next.j].get(0);
    ci.getPos().y = expectedCollisionTable[next.i][next.j].get(1);
    ci.getVel().x = expectedCollisionTable[next.i][next.j].get(2);
    ci.getVel().y = expectedCollisionTable[next.i][next.j].get(3);
    
    if (next.j < objects.size()) { // object-object collision
      CircleMass cj = objects.get(next.j);

      // update position, velocity (of the second object)
      cj.getPos().x = expectedCollisionTable[next.i][next.j].get(4);
      cj.getPos().y = expectedCollisionTable[next.i][next.j].get(5);
      cj.getVel().x = expectedCollisionTable[next.i][next.j].get(6);
      cj.getVel().y = expectedCollisionTable[next.i][next.j].get(7);
      
      //System.out.println(ci);
      //System.out.println(cj);
      
      Collision.collide(cr2Objects, ci, cj); // updates velocity after the collision
      
      //System.out.println(ci);
      //System.out.println(cj);
      
      updateAcceleration(cj);
    } else { // object-border collision
      Border b = Border.TOP;
      if (next.j == objects.size() + 1) {
        b = Border.BOTTOM;
      } else if (next.j == objects.size() + 2) {
        b = Border.LEFT;
      } else if (next.j == objects.size() + 3) {
        b = Border.RIGHT;
      }
      
      Collision.collideWithBorder(crBorder, b, ci);
    }
    
    updateAcceleration(ci);

    // remove entries from nextCollisionSet with one of the objects from this collision
    nextCollisionSet = nextCollisionSet
        .stream()
        .filter(t -> (t.i != next.i && t.j != next.i && (next.j >= objects.size() || (t.i != next.j && t.j != next.j))))
        .collect(Collectors.toCollection(TreeSet::new));
    
    // now update all the other objects' positions, velocities, and accelerations
    // also update the times in nextCollisionSet
    fastForwardByT(next.t, next.i, next.j);
    // now fast forward by a small amount to prevent a second collision from happening
    // this time, include i and j in the fast forwarding
    //fastForwardByT(SMALL_POST_COLLISION_TIME, -1, -1);
    
    // now recalculate expected collision times using updated values
    calcExpectedCollisionsForI(next.i, false, next.j);
    if (next.j < objects.size())
      calcExpectedCollisionsForI(next.j, false, next.i);
    
    return next;
  }
  
  /**
   * Update the positions, velocities, and accelerations to correspond to a time t in the future.
   * @param t the amount of time to go forward by
   * @param skipI skip recalculating for this index
   * @param skipJ skip recalculating for this index
   */
  private void fastForwardByT(double t, int skipI, int skipJ) {
    for (int k = 0; k < objects.size(); k++) {
      if (k == skipI || k == skipJ) continue;
      
      CircleMass ck = objects.get(k);
      ck.getPos().x = CollisionDetection.getPosition(ck.getX(), ck.getVx(), ck.getAx(), t);
      ck.getPos().y = CollisionDetection.getPosition(ck.getY(), ck.getVy(), ck.getAy(), t);
      ck.getVel().x = CollisionDetection.getVelocity(ck.getVx(), ck.getAx(), t);
      ck.getVel().y = CollisionDetection.getVelocity(ck.getVy(), ck.getAy(), t);
      updateAcceleration(ck);
    }
    
    // update the times in nextCollisionSet
    nextCollisionSet.stream().forEach(tup -> tup.t -= t);
  }
  
  /**
   * Run the simulation, outputting intermediate positions, velocities, and accelerations through the output handler.
   * @param delT the amount of simulated time to pass between outputting results
   * @param tMax the total simulated time, after which the function returns. If this value is negative, it never terminates.
   * @param out the output handler on which handleOutput and finalizeOutput will be called
   */
  public void runSimulation(double delT, double tMax, CollisionTrackerOutput out) {//, boolean inRealTime) { 
    //ArrayList<Long> nanosPerFrame = new ArrayList<>();
    //Instant clockStart = Instant.now();
    
    simT = 0.0;
    while(simT < tMax) {
      //System.out.println(" ****************** ");
      //System.out.println(simT);
      //printTables();
      
      double endT = simT + delT;

      while (simT + nextCollisionSet.first().t <= endT) {
        simT += nextCollisionSet.first().t;// + SMALL_POST_COLLISION_TIME;
        
        recalculateNextCollision();
      }
      
      // update all positions to the desired end time
      fastForwardByT(endT - simT, -1, -1);
      
      // pause execution until the clock catches up with the simulation
      //if (inRealTime) {
      //  while (Duration.between(clockStart, Instant.now()).toNanos() / 1000000000.0 < endT) {
      //    try {
      //      Thread.sleep(0, 100000); // 0.1 ms (100 microseconds)
      //    } catch (InterruptedException e) {}
      //  }
      //}
        
      out.handleOutput(this);
      simT = endT;
      
      //Instant frameEnd = Instant.now();
      //nanosPerFrame.add(Duration.between(clockStart, frameEnd).toNanos());
    }
    
    //System.out.println(nanosPerFrame);
    
    out.finalizeOutput(this);
  }
}
