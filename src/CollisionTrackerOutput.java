/**
 * @author Rob Mina (rob.mina@charchrist.com)
 * 
 * CollisionTrackerOutput.java
 * Jul 16, 2020
 */

/**
 * An interface to handle output from the CollisionTracker.
 */
public interface CollisionTrackerOutput {
  /**
   * Called when the CollisionTracker is initialized
   * @param t the CollisionTracker whose output will be handled
   */
  public void initializeForOutput(CollisionTracker t);
  
  /**
   * Called when the CollisionTracker has finished an intermediate step of the simulation.
   * @param t the CollisionTracker whose output will be handled
   */
  public void handleOutput(CollisionTracker t);
  
  /**
   * Called when the runSimulation function reaches the maximum time.
   * @param t the CollisionTracker whose output will be handled
   */
  public void finalizeOutput(CollisionTracker t);
}
